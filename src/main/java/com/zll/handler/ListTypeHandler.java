package com.zll.handler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.springframework.util.StringUtils;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @description: 自定义类型处理器(将字符串转换为List集合)
 * 必须继承BaseTypeHandler，明确指定泛型
 * @author: 八亿科技
 * @date: 2024/08/26 19:29
 */
public class ListTypeHandler extends BaseTypeHandler<List<String>> {
    /**
     * 将传进来的List<String>拼接为String,然后设置到数据库中(需要借助sql参数),对象到数据库
     * @param ps
     * @param i
     * @param parameter
     * @param jdbcType
     * @throws SQLException
     */
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, List<String> parameter, JdbcType jdbcType) throws SQLException {
        StringBuilder stringBuilder = new StringBuilder();
        if (parameter != null && parameter.size() >0) {
            for (String s: parameter) {
                stringBuilder.append(s).append(",");
            }
        }
        ps.setString(i, stringBuilder.toString());
    }

    /**
     * 数据库到对象
     * @param rs
     *          the rs
     * @param columnName
     *          Colunm name, when configuration <code>useColumnLabel</code> is <code>false</code>
     * @return
     * @throws SQLException
     */
    @Override
    public List<String> getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String values = rs.getString(columnName);
        return this.parseList(values);
    }

    /**
     * 数据库到对象
     * @param rs
     * @param columnIndex
     * @return
     * @throws SQLException
     */
    @Override
    public List<String> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        String values = rs.getString(columnIndex);
        return this.parseList(values);
    }

    /**
     * 数据库到对象
     * @param cs
     * @param columnIndex
     * @return
     * @throws SQLException
     */
    @Override
    public List<String> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        String values = cs.getString(columnIndex);
        return this.parseList(values);
    }

    /**
     * 将String转为List<String>
     * @param values
     * @return
     */
    private List<String> parseList(String values) {
        List<String> list = new ArrayList<>();
        if (Objects.nonNull(values)) {
            String[] split = values.split(",");
            if (Objects.nonNull(split) && split.length > 0 ) {
                for (String s : split) {
                    if (StringUtils.hasText(s)) {
                        list.add(s);
                    }
                }
            }
        }
        return list;
    }
}

package com.zll.mapper.employee;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zll.entity.employee.Employee;
import org.apache.ibatis.annotations.Mapper;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/08/26 22:16
 */
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {
}

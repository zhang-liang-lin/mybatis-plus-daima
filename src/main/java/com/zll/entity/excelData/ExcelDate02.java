package com.zll.entity.excelData;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @ExcelProperty 导出的列名
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExcelDate02 {
    @ExcelProperty({"方正镁业有限公司销售结算单","学生ID"})
    private  Integer id;
    @ExcelProperty({"方正镁业有限公司销售结算单","学生姓名"})
    private String name;
    @ExcelProperty({ "方正镁业有限公司销售结算单", "学生年龄" })
    private Integer age;
    @ExcelProperty({ "方正镁业有限公司销售结算单", "生日" })
    private Date birth;
}

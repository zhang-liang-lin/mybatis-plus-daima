package com.zll.entity.excelData;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @ExcelProperty 导出的列名
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExcelDate {
    @ExcelProperty("员工编号")
    private Integer id;
    @ExcelProperty("员工姓名")
    private String name;
    @ExcelProperty("员工年龄")
    private Integer age;
    @ExcelProperty("员工薪资")
    private Double salary;
    @ExcelProperty("入职日期")
    private Date inDate;
}

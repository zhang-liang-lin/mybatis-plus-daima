package com.zll.entity.excelData;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/12/20 09:24:05
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SaleSettlement {
    //货物名称
    private String productName;
    //车数
    private String carSum;
    //结算数量
    private String settlementNumber;
    //结算单价
    private String settlementPrice;
    //结算金额(不含税)
    private String settlementMoney;
    //税额
    private String theAmountOfTaxPayable;
    //结算金额(含税)
    private String settlementAmount;
    //备注
    private String remarks;
}

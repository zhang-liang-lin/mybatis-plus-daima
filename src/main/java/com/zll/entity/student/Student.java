package com.zll.entity.student;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/08/28 11:15:41
 */
@Data
@Builder
public class Student implements Serializable {
    @TableId
    private Long id;
    private String name;
}

package com.zll.entity.employee;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zll.annotation.compare.Compare;
import com.zll.handler.ListTypeHandler;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/08/26 22:15
 */

/**
 * 使用字段类型处理器时，必须开启映射注解 @TableName(autoResultMap = true)。否则插入没问题，但查询时该字段会为空
 */
@Data
@TableName(value = "Employee", autoResultMap = true)
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
    private Long id;

    @Compare("姓名")
    @ApiModelProperty("姓名1")
    private String name;

    @Compare("年龄")
    private Integer age;

    @TableField(typeHandler = ListTypeHandler.class)
    @Compare("手机号")
    @ApiModelProperty("手机号1")
    private List<String> phone = new ArrayList<>();
}

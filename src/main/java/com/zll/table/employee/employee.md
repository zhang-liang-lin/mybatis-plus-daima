create table employee
(
id    bigint auto_increment comment 'id'
primary key,
name  varchar(100) null comment '姓名',
age   int          null comment '年龄',
phone varchar(100) null comment '手机号'
)
comment '员工表' collate = utf8mb4_unicode_ci
row_format = DYNAMIC;

INSERT INTO employee (id, name, age, phone) VALUES (1, '若兰', 32, '12345678901,12345678902,12345678903,');

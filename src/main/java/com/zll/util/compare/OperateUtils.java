package com.zll.util.compare;

import com.zll.util.date.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;

/**
 * @description: 操作记录工具类(对比修改前后的值)
 * @author: 八亿科技
 * @date: 2024/08/28 17:31:31
 */
public class OperateUtils {
    private static Logger logger = LoggerFactory.getLogger(OperateUtils.class);

    /**
     * 判断同一类型的两个对象中属性值发生改变
     *
     * @param oldBean
     * @param newBean
     * @param <T>
     * @return
     */
    public static <T> String compareProperty(T oldBean, T newBean) {
        StringBuilder builder = new StringBuilder();
        try {
            //获取class对象方法
            Class<?> clazz = oldBean.getClass();
            //获取某个类中声明的所有字段，包括公有、私有、受保护的字段，但不包括继承的字段
            Field[] declaredFields = clazz.getDeclaredFields();
            for (Field field : declaredFields) {
                // 排除序列化属性
                if ("serialVersionUID".equals(field.getName())) {
                    continue;
                }
                //获取对应属性的get,set方法
                PropertyDescriptor pd = new PropertyDescriptor(field.getName(), clazz);
                // 获取对应属性的get方法
                Method getMethod = pd.getReadMethod();
                //根据get方法获取到属性值
                Object o1 = getMethod.invoke(oldBean);
                Object o2 = getMethod.invoke(newBean);
                if ((o1 == null && o2 == null) || (ObjectUtils.isEmpty(o1) && ObjectUtils.isEmpty(o2))) {
                    continue;
                }
                //获取到有此注解的成员变量(属性)
                ApiModelProperty property = field.getAnnotation(ApiModelProperty.class);
                if (property == null) {
                    continue;
                }

                if (o1 != null && o2 != null) {
                    if (o1 instanceof String[] && o2 instanceof String[]) {
                        String[] arr1 = (String[]) o1;
                        String[] arr2 = (String[]) o2;
                        if (!Arrays.equals(arr1, arr2)) {
                            //property.value() 获得注解对应的属性名
                            builder.append("将"
                                    + property.value()
                                    + "由"
                                    + (ObjectUtils.isEmpty(arr1) ? " - " : Arrays.asList(arr1)) + "修改为"
                                    + (ObjectUtils.isEmpty(arr2) ? " - " : Arrays.asList(arr2) + "；"));
                        }
                    } else {
                        if (!o1.toString().equals(o2.toString())) {
                            if (!ObjectUtils.isEmpty(o1) && o1 instanceof Date) {
                                o1 = DateUtils.DateToTimeStr((Date) o1);
                            }
                            if (!ObjectUtils.isEmpty(o2) && o2 instanceof Date) {
                                o2 = DateUtils.DateToTimeStr((Date) o2);
                            }
                            builder.append("将"
                                    + property.value()
                                    + "由"
                                    + (ObjectUtils.isEmpty(o1) ? " - " : o1) + "修改为"
                                    + (ObjectUtils.isEmpty(o2) ? " - " : o2) + "；");
                        }
                    }
                }
                if ((o1 == null && o2 != null) || (o1 != null && o2 == null)) {
                    if (o1 instanceof String[] || o2 instanceof String[]) {
                        String[] arr1 = null;
                        String[] arr2 = null;
                        if (o1 != null && o2 == null && o1 instanceof String[]) {
                            arr1 = (String[]) o1;
                        }
                        if (o1 == null && o2 != null && o2 instanceof String[]) {
                            arr2 = (String[]) o2;
                        }
                        builder.append("将"
                                + property.value()
                                + "由"
                                + (ObjectUtils.isEmpty(arr1) ? " - " : Arrays.asList(arr1)) + "修改为"
                                + (ObjectUtils.isEmpty(arr2) ? " - " : Arrays.asList(arr2)) + "；");
                    } else {
                        if (!ObjectUtils.isEmpty(o1) && o1 instanceof Date) {
                            o1 = DateUtils.DateToTimeStr((Date) o1);
                        }
                        if (!ObjectUtils.isEmpty(o2) && o2 instanceof Date) {
                            o2 = DateUtils.DateToTimeStr((Date) o2);
                        }
                        builder.append("将"
                                + property.value()
                                + "由"
                                + (ObjectUtils.isEmpty(o1) ? " - " : o1) + "修改为"
                                + (ObjectUtils.isEmpty(o2) ? " - " : o2) + "；");
                    }
                }
            }
        } catch (Exception e) {
            logger.error("OperateUtils.compareProperty is error", e);
        }
        String content = builder.toString();
        if (StringUtils.hasText(content) && content.endsWith("；")) {
            content = content.substring(0, content.length() - 1);
        }
        return content;
    }
}

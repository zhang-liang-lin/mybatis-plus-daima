package com.zll.util.json;

import com.alibaba.fastjson.JSONObject;
import com.zll.entity.employee.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/11/20 13:41:50
 */
@Component
@Slf4j
public class JSONObjectUtils {
    /**
     * String字符串转json
     * @param jsonString
     */
    public static JSONObject StringToJson(String jsonString) {
        JSONObject jsonObject = JSONObject.parseObject(jsonString);
        return jsonObject;
    }

    /**
     * json转String字符串
     * @param jsonObject
     */
    public static String JsonToString(JSONObject jsonObject) {
        String jsonString = JSONObject.toJSONString(jsonObject);
        return jsonString;
    }

    /**
     * Java对象-->JSON对象
     * @param employee
     * @return
     */
    public static JSONObject javaObjectToJsonObject(Employee employee) {
        JSONObject  jsonObject = (JSONObject) JSONObject.toJSON(employee);
        return jsonObject;
    }

    /**
     * Java对象-->JSON字符串
     * @param employee
     * @return
     */
    public static String javaObjectToJsonString(Employee employee) {
        String jsonString = JSONObject.toJSONString(employee);
        return jsonString;
    }

    /**
     * JSON对象-->Java对象
     * @param jsonObject
     * @return
     */
    public static Employee jsonObjectToJavaObject(JSONObject jsonObject) {
        Employee employee = JSONObject.toJavaObject(jsonObject, Employee.class);
        return employee;
    }

    /**
     * JSON字符串->Java对象
     * @param jsonString
     * @return
     */
    public static Employee jsonStringToJavaObject(String jsonString) {
        Employee employee = JSONObject.parseObject(jsonString, Employee.class);
        return employee;
    }

    /**
     * JSON字符串->List<Java对象>
     * @param jsonString
     * @return
     */
    public static  List<Employee> jsonStringToListJavaObject(String jsonString) {
        List<Employee> employees = JSONObject.parseArray(jsonString, Employee.class);
        return employees;
    }

    /**
     * 判断json对象中的属性是否存在
     */
    public static void containsKey() {
        // 创建一个 JSONObject 对象
        JSONObject jsonObject = new JSONObject();
        // 添加属性
        jsonObject.put("name", "Bob");
        jsonObject.put("age", 28);
        // 修改属性值
        jsonObject.put("age", 30);
        // 删除属性
        jsonObject.remove("name");

        // 判断属性是否存在
        if (jsonObject.containsKey("name")) {
            System.out.println("Name: " + jsonObject.getString("name"));
        } else {
            System.out.println("Name not found.");
        }

        // 转换为 JSON 字符串
        String jsonString = jsonObject.toJSONString();

        System.out.println("Modified JSON string: " + jsonString);
    }
}

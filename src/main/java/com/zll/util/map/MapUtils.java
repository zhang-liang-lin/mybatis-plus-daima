package com.zll.util.map;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/10/14 13:43:09
 */
@UtilityClass
@Slf4j
public class MapUtils {


    /**
     * compute 如果key存在,对key对应的value进行修改, key不存在,报空指针
     */
    public static void compute() {
        Map<String, Integer> map = new HashMap<>();
        map.put("1", 1);
        map.put("3", 3);
        map.compute("1", (k, v) -> v + 5);
        map.compute("2", (k, v) -> k == null ? 1 : v + 1);
//        map.compute("2", (k, v) -> v + 5);
        map.forEach((k, v) -> System.out.println(k + ":" + v));
    }

    /**
     * computeIfPresent 如果key存在,对key对应的value进行修改, key不存在,不进行操作
     * 相当于注释掉的内容
     */
    public static void computeIfPresent() {
        Map<String, Integer> map = new HashMap<>();
        map.put("1", 1);
        map.put("3", 3);
//        if (map.containsKey("1")) {
//            map.put("1", map.get("1") + 1);
//        }
        map.computeIfPresent("1", (k, v) -> v + 1);
        map.computeIfPresent("2", (k, v) -> v + 1);
        map.forEach((k, v) -> System.out.println(k + ":" + map.get(k)));
    }




    /**
     * computeIfAbsent  用来从map中获取key对应的value 如果key不存在,将key添加到map中,并给一个自定义的value值   如果key存在,不进行任何操作  类似于注释掉的内容
     * key 键
     * Function<? super K, ? extends V> mappingFunction  自定义的value值
     */
    /**
     * 检查现有值：首先检查指定的键是否已经存在于map中。
     * 生成新值：如果键不存在，使用mappingFunction生成一个新的值。
     * 存储新值：将新生成的值与键关联并存储在map中。
     * 返回值：返回与键关联的值（新生成的或已存在的）。
     */
    public static void computeIfAbsent() {
        String text = "hello world hello Java hello world";
        String[] words = text.split(" ");
        Map<String, Integer> wordCount = new HashMap<>();
        for (String word : words) {
            //给map中设置默认的value,为0
            wordCount.computeIfAbsent(word, k -> 0);
            wordCount.put(word, wordCount.get(word) + 1);
        }
//        if (!map.containsKey("a")) {
//            map.put("a", 0);
//        }
        for (int i = 0; i < 2; i++) {
            wordCount.computeIfAbsent(i+"", k -> 0);
            wordCount.put(i+"", wordCount.get(i+""));
        }
        wordCount.forEach((k, v) -> System.out.println(k + ": " + v));
    }



    /**
     * values 获取map中所有的值
     * isEmpty 判断map是否为null
     * clear 清除map的key和value
     * putAll 将多个key,value封装成新的map放入map中
     * replaceAll 对所有的value进行修改
     * replace 对指定的key修改value
     */
    public static void test() {
        Map<String, Integer> map = new HashMap<>();
        map.put("1", 1);
        map.put("3", 3);
        System.out.println(map);
        List<Integer> list = map.values().stream().collect(Collectors.toList());
        System.out.println(list);
        boolean empty = map.isEmpty();
        System.out.println(empty);
        System.out.println(map.hashCode());
        Map<String, Integer> map1 = new HashMap<>();
        map1.put("2", 5);
        map1.put("4", 0);
        map.putAll(map1);
        System.out.println(map);
        map.replaceAll((k, v) -> v + 6);
        System.out.println(map);
        map.replace("2", 100);
        System.out.println(map);
        //如果旧的value一致,将value修改为新的
        map.replace("2", 200, 100);
        System.out.println(map);
        map.replace("2", 100, 200);
        System.out.println(map);
        map.clear();
        System.out.println(map);
        Map<String, Integer> map2 = new HashMap<>();
        map2.put("test1", 1);
        map2.merge("test1", 2, Integer::sum);
        map2.merge("test2", 2, Integer::sum);
        System.out.println(map2);
        /**
         * merge:
         *  key 键
         *  value 值 如果值不是key对应的值,直接替换  如果值是key对应的值,进行合并,合并规则为remappingFunction
         *  remappingFunction: (v1, v2) -> v1 + v2
         */
        map2.merge("test1", 1, (v1, v2) -> v1 + v2);
        map2.merge("test2", 3, (v1, v2) -> v1 + v2); //两个值相加
        System.out.println(map2);
        map2.merge("test2", 3, (v1, v2) -> v1); // 取旧值 key对应的原本的value
        System.out.println(map2);
        map2.merge("test2", 3, (v1, v2) -> v2); //取新值 value
        System.out.println(map2);
    }

}

package com.zll.util.http.vo;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/09/20 09:26:53
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.io.Serializable;
import java.util.Objects;

/**
 * 通用返回
 *
 * @param <T> 数据泛型
 */
@Data
public class CommonResultVO<T> implements Serializable {

    /**
     * 错误码
     *
     */
    private Integer code;
    /**
     * 返回数据
     */
    private T data;
    /**
     * 错误提示，用户可阅读
     *
     */
    private String msg;



    public static boolean isSuccess(Integer code) {
        return Objects.equals(code, CommonCodeEnum.SUCCESS.getCode());
    }

    @JsonIgnore // 避免 jackson 序列化
    public boolean isSuccess() {
        return isSuccess(code);
    }

    @JsonIgnore // 避免 jackson 序列化
    public boolean isError() {
        return !isSuccess();
    }



    @AllArgsConstructor
    @Getter
    enum CommonCodeEnum {
        SUCCESS(0, "成功"),
        ERROR(1, "失败"),
        ;

        private final Integer code;

        private final String message;
    }

}

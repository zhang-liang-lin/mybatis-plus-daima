package com.zll.util.http.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/09/20 09:24:01
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "部门组织返回简单数据模型")
public class DeptSimpleRespVO implements Serializable {

    @ApiModelProperty(value = "部门编号", required = true, example = "1024")
    private Long id;

    @ApiModelProperty(value = "部门名称", required = true, example = "芋道")
    private String name;

    @ApiModelProperty(value = "父部门 ID", required = true, example = "1024")
    private Long parentId;

}

package com.zll.util.http.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @description: 采购订单
 * @author: 八亿科技
 * @date: 2024/07/31 14:54:06
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PurchaseOrderReqVO implements Serializable {

    /**
     * 采购订单ID
     */
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;
}

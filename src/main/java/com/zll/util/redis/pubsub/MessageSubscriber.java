package com.zll.util.redis.pubsub;

import org.springframework.stereotype.Component;

/**
 * @description: 消息订阅者
 * @author: 八亿科技
 * @date: 2025/02/14 15:21:22
 */
@Component
public class MessageSubscriber {

    public void onMessage(String message, String channel) {
        System.out.println("Received message from channel " + channel + ": " + message);
    }
}

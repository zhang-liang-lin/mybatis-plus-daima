package com.zll.util.redis.pubsub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @description: 消息发布者 用于发送消息到特定的频道
 * @author: 八亿科技
 * @date: 2025/02/14 15:18:06
 */
@Component
public class MessagePublisher {

    @Autowired
    private RedisTemplate redisTemplate;

    public void publish(String channel, String message) {
        redisTemplate.convertAndSend(channel, message);
        System.out.println("Message published to channel " + channel + ": " + message);
    }
}

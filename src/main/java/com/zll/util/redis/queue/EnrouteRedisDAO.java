package com.zll.util.redis.queue;


import com.zll.util.http.vo.PurchaseOrderReqVO;
import com.zll.util.redis.RedisUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Map;


/**
 * 在途排队缓存
 *
 * @author 八亿科技
 */
@Repository
public class EnrouteRedisDAO {

    @Resource
    private RedisUtils redisUtils;

    public void set(String waybillNo) {
        PurchaseOrderReqVO purchaseOrderReqVO = new PurchaseOrderReqVO();
        purchaseOrderReqVO.setTenantId(87L);
        purchaseOrderReqVO.setId(111L);
        if (!StringUtils.hasText(waybillNo)) {
            return;
        }
        String key = "enroute" + ":" + 87 + ":" + "queueup";
        /**
         * hset: hash哈希
         * HSET key field value
         * key:类似java中的对象 field:类似java中的属性名 value:似java中的属性值
         */
        redisUtils.hset(key, waybillNo, purchaseOrderReqVO);
    }

    public Object get(String item) {
        if (!StringUtils.hasText(item)) {
            return null;
        }
        String key = "enroute" + ":" + 87 + ":" + "queueup";
        System.out.println(redisUtils.hget(key, item));
        return redisUtils.hget(key, item);
    }

    public Map<Object, Object> scan() {
        String key = "enroute" + ":" + 87 + ":" + "queueup";
        return redisUtils.hmget(key);
    }

    public void del(String item) {
        if (!StringUtils.hasText(item)) {
            return;
        }
        String key = "enroute" + ":" + 87 + ":" + "queueup";
        redisUtils.hdel(key, item);
    }
}

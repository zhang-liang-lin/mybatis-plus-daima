package com.zll.util.redis;

import com.zll.util.redis.pubsub.MessageSubscriber;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;


@Configuration
public class RedisConfig {

    /**
     * bean的注入
     * @param redisConnectionFactory
     * @return
     */
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {

        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();

        //使用Jackson2JsonRedisSerializer来序列化和反序列化redis的value值
        redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
        redisTemplate.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());

        //使用StringRedisSerializer来序列化和反序列化redis的key值
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());

        //开启事务
        redisTemplate.setEnableTransactionSupport(true);

        redisTemplate.setConnectionFactory(redisConnectionFactory);

        return redisTemplate;
    }

    @Bean
    public RestTemplate restTemplate(){
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        //设置连接主机超时时间
        factory.setConnectTimeout(5*1000);
        //设置从主机读取数据超时时间
        factory.setReadTimeout(5*1000);
        return new RestTemplate(factory);
    }

    /**
     * 配置Redis的消息监听容器，将监听器适配器绑定到指定的频道，以便接收和处理来自该频道的消息。用于实现发布-订阅模式，当有消息发布到"myChannel"时，监听器会处理这些消息
     * 步骤：1.创建监听容器；2.注入连接工厂；3.注册监听器到指定频道
     * @param connectionFactory
     * @param listenerAdapter
     * @return
     */
    @Bean
    public RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory,
                                                   MessageListenerAdapter listenerAdapter) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.addMessageListener(listenerAdapter, new ChannelTopic("myChannel"));
        return container;
    }

    /**
     * 创建消息监听适配器，将Redis消息转发给MessageSubscriber的onMessage方法处理。通过@Bean将适配器注入Spring容器，实现消息订阅功能
     * MessageListenerAdapter是Spring Data Redis中的一个类，用于将消息监听适配到指定的处理方法上
     * 逻辑是将接收到的Redis消息转发给MessageSubscriber的onMessage方法处理
     * 作用是配置一个消息监听适配器，让订阅者通过onMessage方法来处理来自Redis的消息
     * @param subscriber
     * @return
     */
    @Bean
    public MessageListenerAdapter listenerAdapter(MessageSubscriber subscriber) {
        return new MessageListenerAdapter(subscriber, "onMessage");
    }
}

package com.zll;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/08/25 14:24
 */
@SpringBootApplication
@MapperScan(basePackages = "com.zll.mapper")
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}

package com.zll.subscribe;

import com.zll.entity.student.Student;
import lombok.Builder;
import lombok.Data;

/**
 * @description: 学生事件
 * 事件（Event）：也称为消息（Message），是一个对象，包含了要传递的信息。事件可以是任意类型的对象，但通常是一个简单的POJO（Plain Old Java Object）
 * @author: 八亿科技
 * @date: 2024/10/09 14:09:42
 */

/**
 * EventBus的优势
 * 1.简化组件之间的通讯方式：通过事件发布和订阅的方式，使得组件间的通信更加简单和直观。
 * 2.解耦：事件的发布者和订阅者之间不需要直接引用，降低了组件之间的耦合度。
 * 3.灵活的线程模式：EventBus提供了多种线程模式，允许开发者根据需要选择合适的工作线程。
 * 4.高性能：EventBus内部使用高效的数据结构和算法，确保了事件传递的高效性。
 * 5.易于使用：通过简单的API调用，即可实现事件的发布和订阅。
 */

/**
 * EventBus的使用步骤
 * 1.定义事件
 * 2.注册订阅者
 * 3.实现事件处理方法
 * 4.发布事件
 */
@Data
@Builder
public class StudentEvent {
    private Student student;
}

package com.zll.subscribe;

import com.zll.entity.student.Student;
import lombok.extern.slf4j.Slf4j;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.springframework.stereotype.Component;

/**
 * @description: 学生事件订阅
 * @author: 八亿科技
 * @date: 2024/10/09 14:10:51
 */
@Component
@Slf4j
public class StudentSubscribe {

    /**
     * 注册订阅者
     */
    public StudentSubscribe() {
        EventBus.getDefault().register(this);
    }


    /**
    * EventBus的线程模式
     * POSTING：事件处理方法将在发布事件的线程中调用，这是默认模式。
     * MAIN：事件处理方法将在Android的主线程（UI线程）中调用。如果发布线程是主线程，则直接调用；如果不是，则事件将被排入队列等待主线程处理。
     * MAIN_ORDERED：与MAIN模式类似，但事件处理将具有更严格和更一致的顺序。
     * BACKGROUND：事件处理方法将在后台线程中调用。如果发布线程是主线程，EventBus将使用单独的后台线程来处理事件；如果发布线程已经是后台线程，则直接在该线程中调用事件处理方法。
     * ASYNC：事件处理方法将在单独的线程中调用，这个线程既不是主线程也不是发布事件的线程。这适用于耗时的操作
    */

    /**
     * EventBus的注意事项
     * 反射性能问题：EventBus在注册时会使用反射来遍历注册对象的方法，以找出带有@Subscribe注解的方法，这可能会影响性能
     * 内存泄漏：如果忘记在适当的生命周期方法中调用unregister方法，订阅者将不会被垃圾回收器回收，从而导致内存泄漏。
     * 混淆配置：在使用ProGuard等代码混淆工具时，需要配置相应的混淆规则，以确保EventBus正常工作
     */

    /**
     * 事件的订阅(实现事件处理方法)
     * 订阅者（Subscriber）：订阅了某个或某些事件的类。订阅者需要实现事件处理方法，并在该方法上添加@Subscribe注解
     * @param event 事件
     */
    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void subscribe(StudentEvent event) {
        Student student = event.getStudent();
        String name = student.getName();
        String ttsParam = "{\"name\":\"" + name + "\"}";
        System.out.println(ttsParam);
        String notifyContent = name + "您的排号状态已变更为已叫号，请及时入场";
        System.out.println(notifyContent);
    }
}

package com.zll.service.student;

import com.zll.entity.student.Student;
import com.zll.subscribe.StudentEvent;
import org.greenrobot.eventbus.EventBus;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/10/09 14:26:33
 */
@Service
public class StudentServiceImpl implements StudentService{
    @Override
    public void studentEvent() {
        Student student = Student.builder().id(1L).name("yrl").build();
        StudentEvent studentEvent = StudentEvent.builder().student(student).build();
        //发布事件 根据发布的事件找到对应的订阅者,并执行对应的事件
        //发布者（Publisher）：发布事件的类。发布者可以通过调用EventBus的post方法发布事件
        EventBus.getDefault().post(studentEvent);
    }
}

package com.zll.service.http;

import cn.hutool.json.JSONUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.zll.util.http.vo.CommonResultVO;
import com.zll.util.http.vo.PurchaseOrderReqVO;
import com.zll.util.json.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @description: 如果需要使用RestTemplate,先需要通过bean注入
 * @author: 八亿科技
 * @date: 2024/09/20 10:25:23
 */
@Service
@Slf4j
public class HttpServiceImpl implements HttpService{

    @Resource
    private RestTemplate restTemplate;

    @Override
    public void httpTest() {
        PurchaseOrderReqVO purchaseOrderReqVO = new PurchaseOrderReqVO();
        purchaseOrderReqVO.setId(1L);
        purchaseOrderReqVO.setTenantId(87L);
        // JSONUtil.toJsonStr(purchaseOrderReqVO) 把对象转为Json字符串
        log.info("PurchaseOrderRpc#getPurchaseOrderStatus:{}", JSONUtil.toJsonStr(purchaseOrderReqVO));
        System.out.println(JSONUtil.toJsonStr(purchaseOrderReqVO));
        String url = "http://test.8ykj.cn:48081/admin-api/inventory/purchase-order/get-status";
        // 创建HttpHeaders对象并添加请求头
        HttpHeaders headers = new HttpHeaders();
        //自定义请求头
        headers.set("tenant-id", purchaseOrderReqVO.getTenantId().toString());
        //ContentType 文本类型  MediaType.APPLICATION_JSON_UTF8   application/json:JSON数据格式
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        // 创建HttpEntity对象 封装了请求头和请求体
        // HttpEntity表示http的request和response实体，它由消息头和消息体组成
        HttpEntity<?> entity = new HttpEntity<>(purchaseOrderReqVO, headers);
        /**
         * url – url地址
         * method – 请求方法
         * requestEntity – 请求实体
         * responseType – 响应类型
         * uriVariables – 暂未用到
         */
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        // response.getStatusCode() : 响应码和响应信息 200 OK
        // response.getBody() 响应内容 {"code":0,"data":null,"msg":""}
        // response.getStatusCodeValue() 响应码 200
        // response.getHeaders() 响应头
        log.info("PurchaseOrderRpc#getPurchaseOrderStatus#response:{}", response);
        String errorMessage = "获取采购订单状态异常！";
        if (!HttpStatus.OK.equals(response.getStatusCode()) || Objects.isNull(response.getBody())) {
            throw new RuntimeException(errorMessage);
        }
        System.out.println(response.getBody());
        CommonResultVO<Integer> result = JsonUtils.parseObject(response.getBody(), new TypeReference<CommonResultVO<Integer>>() {
        });
        System.out.println(result);
        if (Objects.isNull(result) || result.isError()) {
            throw new RuntimeException(StringUtils.isBlank(result.getMsg()) ? errorMessage : result.getMsg());
        }
        System.out.println(result.getData());
    }
}

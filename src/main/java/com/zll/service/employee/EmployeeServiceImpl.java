package com.zll.service.employee;

import com.zll.mapper.employee.EmployeeMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/08/26 19:20
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Resource
    private EmployeeMapper employeeMapper;
    @Override
    public void selectList() {
        employeeMapper.selectList(null);
    }
}

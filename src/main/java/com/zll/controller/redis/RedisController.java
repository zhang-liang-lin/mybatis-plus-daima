package com.zll.controller.redis;

import com.zll.util.redis.pubsub.MessagePublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: redis pub/sub 测试
 * @author: 八亿科技
 * @date: 2025/02/14 16:22:34
 */
@RestController
@RequestMapping("/system/redis")
public class RedisController {
    @Autowired
    private MessagePublisher messagePublisher;

    @GetMapping("/publish")
    public String publishMessage(@RequestParam String message) {
        messagePublisher.publish("myChannel", message);
        return "Message published!";
    }
}

package com.zll.controller.employee;

import com.zll.entity.employee.Employee;
import com.zll.mapper.employee.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/08/26 21:47
 */
@RestController
public class EmployeeController {

    @Autowired
    EmployeeMapper employeeMapper;

    @RequestMapping("/insert")
    public void insert(){
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setName("若兰");
        employee.setAge(32);
        employee.setPhone(Arrays.asList("12345678901","12345678902","12345678903"));
        employeeMapper.updateById(employee);

    }
    @RequestMapping("/select")
    public Employee select(){
        return employeeMapper.selectById(1L);
    }
}

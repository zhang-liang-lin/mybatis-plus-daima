package com.zll.controller.http;

import com.zll.service.http.HttpService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/09/20 10:20:58
 */
@RestController
@RequestMapping("/system/dept")
public class HttpController {
    @Resource
    private HttpService httpService;

    @GetMapping("/getEnterpriseDeptList")
    public void getEnterpriseDeptList() {
        httpService.httpTest();
    }
}

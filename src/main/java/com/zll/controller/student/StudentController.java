package com.zll.controller.student;

import com.zll.service.student.StudentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @description: EventBus(基于观察者模式) 发布订阅
 * GET http://localhost:8081/eventbus/get
 * @author: 八亿科技
 * @date: 2024/10/09 14:27:25
 */
@RestController
@RequestMapping("/eventbus")
public class StudentController {

    @Resource
    private StudentService studentService;

    @GetMapping("/get")
    public void get() {
        studentService.studentEvent();
    }
}

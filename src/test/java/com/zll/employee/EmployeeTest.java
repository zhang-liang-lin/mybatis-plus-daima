package com.zll.employee;

import com.zll.controller.employee.EmployeeController;
import com.zll.entity.employee.Employee;
import com.zll.mapper.employee.EmployeeMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/08/26 19:52
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class EmployeeTest {
    @Autowired
    EmployeeMapper employeeMapper;

    @Test
    public void selectTest(){
        Employee employee = employeeMapper.selectById(1L);
        System.out.println(employee);
    }

    @Test
    public void updateTest(){
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setName("若兰");
        employee.setAge(32);
        employee.setPhone(Arrays.asList("12345678901","12345678902","12345678903"));
        employeeMapper.updateById(employee);
    }
}

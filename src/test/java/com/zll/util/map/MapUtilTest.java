package com.zll.util.map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/10/14 14:45:47
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MapUtilTest {
    @Test
    public void test01() {
        MapUtils.computeIfAbsent();
    }

    @Test
    public void test02() {
        MapUtils.computeIfPresent();
    }

    @Test
    public void test03() {
        MapUtils.compute();
    }

    @Test
    public void test04() {
        MapUtils.test();
    }
}

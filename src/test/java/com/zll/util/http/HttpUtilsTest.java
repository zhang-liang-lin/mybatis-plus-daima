package com.zll.util.http;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/09/30 11:07:44
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class HttpUtilsTest {
    @Resource
    private HttpUtils httpUtils;

    @Test
    public void test() {
        httpUtils.httpTest();
    }
}

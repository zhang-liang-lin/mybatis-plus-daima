package com.zll.util.json;

import com.alibaba.fastjson.JSONObject;
import com.zll.entity.employee.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/11/20 14:21:06
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class JsonUtilsTest {
    @Test
    public void test01() {
        String jsonString = "{\"name\": \"John\", \"age\": 30}";
        JSONObject jsonObject = JSONObjectUtils.StringToJson(jsonString);
        System.out.println(jsonObject);
        System.out.println(jsonObject.getString("name"));
        System.out.println(jsonObject.getInteger("age"));
        System.out.println(jsonObject.getIntValue(("age")));
    }

    @Test
    public void test02() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "zhangsan");
        jsonObject.put("age", 26);
        System.out.println(jsonObject);
        System.out.println(jsonObject.getClass());
        System.out.println(JSONObjectUtils.JsonToString(jsonObject));
        System.out.println(JSONObjectUtils.JsonToString(jsonObject).getClass());
    }

    @Test
    public void test04() {
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setName("zhangsan");
        employee.setAge(27);
        ArrayList<String> phones = new ArrayList<>();
        phones.add("12345678901");
        phones.add("12345678902");
        employee.setPhone(phones);
        System.out.println(employee);
        System.out.println(JSONObjectUtils.javaObjectToJsonObject(employee));
    }

    @Test
    public void test05() {
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setName("zhangsan");
        employee.setAge(27);
        ArrayList<String> phones = new ArrayList<>();
        phones.add("12345678901");
        phones.add("12345678902");
        employee.setPhone(phones);
        System.out.println(employee);
        System.out.println(JSONObjectUtils.javaObjectToJsonString(employee));
    }

    @Test
    public void test06() {
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setName("zhangsan");
        employee.setAge(27);
        ArrayList<String> phones = new ArrayList<>();
        phones.add("12345678901");
        phones.add("12345678902");
        employee.setPhone(phones);
        System.out.println(employee);
        JSONObject jsonObject = JSONObjectUtils.javaObjectToJsonObject(employee);
        System.out.println(jsonObject);
        System.out.println(JSONObjectUtils.jsonObjectToJavaObject(jsonObject));
    }

    @Test
    public void test07() {
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setName("zhangsan");
        employee.setAge(27);
        ArrayList<String> phones = new ArrayList<>();
        phones.add("12345678901");
        phones.add("12345678902");
        employee.setPhone(phones);
        System.out.println(employee);
        String jsonString = JSONObjectUtils.javaObjectToJsonString(employee);
        System.out.println(jsonString);
        System.out.println(JSONObjectUtils.jsonStringToJavaObject(jsonString));
    }

    @Test
    public void test08() {
        String employeeString = "[{\"age\":2,\"name\":\"zhangsan\",\"id\":\"1\"},{\"age\":18,\"name\":\"BiggerBoy\",\"id\":\"2\"}]";
        System.out.println(JSONObjectUtils.jsonStringToListJavaObject(employeeString));
    }

    @Test
    public void test03() {
        JSONObjectUtils.containsKey();
    }
}

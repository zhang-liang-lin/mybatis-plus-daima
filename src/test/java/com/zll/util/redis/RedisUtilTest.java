package com.zll.util.redis;

import com.zll.util.http.vo.PurchaseOrderReqVO;
import com.zll.util.redis.queue.EnrouteRedisDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/09/23 15:05:34
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class RedisUtilTest {
    @Resource
    private EnrouteRedisDAO enrouteRedisDAO;
    @Resource
    private RedisUtils redisUtils;

    /**
     * redis设置与获取键值(String)
     */
    @Test
    public void redisTest05() {
        redisUtils.set("20240930", new PurchaseOrderReqVO(123L, 87L));
        PurchaseOrderReqVO purchaseOrderReqVO = (PurchaseOrderReqVO) redisUtils.get("20240930");
        System.out.println(purchaseOrderReqVO.getTenantId() + ":" + purchaseOrderReqVO.getId());
    }

    /**
     * 判断key是否存在
     */
    @Test
    public void redisTest08() {
        System.out.println(redisUtils.hasKey("20241001"));
    }

    /**
     * 设置缓存失效时间
     */
    @Test
    public void redisTest06() {
        redisUtils.expire("20241001", 1000L);
    }

    /**
     * 根据key 获取过期时间 单位为秒
     */
    @Test
    public void redisTest07() {
        System.out.println(redisUtils.getExpire("20241001"));
        System.out.println(redisUtils.getExpire("20241002"));
    }

    /**
     * 删除缓存
     */
    @Test
    public void redisTest09() {
        redisUtils.del("20241002","20241003");
    }

    /**
     * 普通缓存放入并设置时间
     */
    @Test
    public void redisTest10() {
        redisUtils.set("20241002","20241003",100);
    }

    /**
     * 递增
     */
    @Test
    public void redisTest11() {
        redisUtils.incr("20241003",2);
        System.out.println(redisUtils.get("20241003"));
    }

    /**
     * 递减
     */
    @Test
    public void redisTest12() {
        redisUtils.decr("20241003",2);
        System.out.println(redisUtils.get("20241003"));
    }




    /**
     * redis设置键值(hash)
     */
    @Test
    public void redisTest01() {
        enrouteRedisDAO.set("YD20240924");
    }

    /**
     * redis获取键值(hash)
     */
    @Test
    public void redisTest02() {
        enrouteRedisDAO.get("YD20240924");
    }

    /**
     * redis删除键值(hash)
     */
    @Test
    public void redisTest03() {
        enrouteRedisDAO.del("YD20240924");
    }

    /**
     * 获得过期时间
     */
    @Test
    public void redisTest04() {
        System.out.println(redisUtils.getExpire("enroute:87:queueup"));
    }

    /**
     * 根据key获取item(hash) 类似下面的根据String获取map中的String
     * hash，可多个,类似java中的Map<String,Map<String,String>>
     */
    @Test
    public void redisTest13() {
        System.out.println(redisUtils.hKeys("enroute:87:queueup"));
    }

    /**
     * 根据key获取item和value(hash) 类似下面的根据String获取Map
     * hash，可多个,类似java中的Map<String,Map<String,String>>
     */
    @Test
    public void redisTest14() {
        System.out.println(redisUtils.hmget("enroute:87:queueup"));
    }

    /**
     * 向hash中同时插入多个值
     */
    @Test
    public void redisTest15() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("YD20240930",new PurchaseOrderReqVO(117L,87L));
        map.put("YD20241001",new PurchaseOrderReqVO(118L,87L));
        System.out.println(redisUtils.hmset("enroute:87:queueup", map));
    }

    /**
     * 向hash中同时插入多个值并设置过期时间
     */
    @Test
    public void redisTest16() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("YD20241002",new PurchaseOrderReqVO(119L,87L));
        map.put("YD20241003",new PurchaseOrderReqVO(120L,87L));
        System.out.println(redisUtils.hmset("enroute:87:queueup", map, 6000));
    }

    /**
     * 判断hash表中是否有该项的值
     */
    @Test
    public void test17() {
        System.out.println(redisUtils.hHasKey("enroute:87:queueup", "YD20240924"));
//        redisUtils.hset("enroute:87:queueup","YD20240930",1);
    }

    /**
     * hash递增 如果不存在,就会创建一个 并把新增后的值返回
     */
    @Test
    public void test18() {
        System.out.println(redisUtils.hincr("enroute:87:queueup", "YD20240930",3));
        System.out.println(redisUtils.hget("enroute:87:queueup", "YD20240930"));
    }

    /**
     * hash递减
     */
    @Test
    public void test19() {
        System.out.println(redisUtils.hdecr("enroute:87:queueup", "YD20240930",1));
        System.out.println(redisUtils.hget("enroute:87:queueup", "YD20240930"));
    }


    /**
     * 将数据放入set缓存
     * 从set缓存中获取数据
     */
    @Test
    public void test20() {
        System.out.println(redisUtils.sSet("20241002", 1,2,3));
        System.out.println(redisUtils.sGet("20241002"));
    }

    /**
     * 根据value从一个set中查询,是否存在
     */
    @Test
    public void test21() {
        System.out.println(redisUtils.sHasKey("20241002", 1));
    }

    /**
     * 获取set缓存的长度
     */
    @Test
    public void test22() {
        System.out.println(redisUtils.sGetSetSize("20241002"));
    }

    /**
     * 移除set中的值
     */
    @Test
    public void test23() {
        System.out.println(redisUtils.setRemove("20241002",2));
        System.out.println(redisUtils.sGet("20241002"));
    }

    /**
     * 将list放入缓存
     */
    @Test
    public void test24() {
        System.out.println(redisUtils.lSet("20241004", new PurchaseOrderReqVO(189L, 87L)));
    }

    /**
     * 将list放入缓存并获取容器中元素数量,向后插入
     */
    @Test
    public void test25() {
        System.out.println(redisUtils.rightPush("20241004", new PurchaseOrderReqVO(199L, 87L)));
    }

    /**
     * 获取list缓存的内容
     */
    @Test
    public void test26() {
        System.out.println(redisUtils.lGet("20241004", 0, -1));
    }

    /**
     * 获取list缓存的长度
     */
    @Test
    public void test27() {
        System.out.println(redisUtils.lGetListSize("20241004"));
    }

    /**
     * 通过索引 获取list中的值
     */
    @Test
    public void test28() {
        System.out.println(redisUtils.lGetIndex("20241004", 1));
    }

    /**
     * 将list放入缓存
     */
    @Test
    public void test29() {
        List<PurchaseOrderReqVO> purchaseOrderReqVOS = new ArrayList<>();
        purchaseOrderReqVOS.add(new PurchaseOrderReqVO(321L, 88L));
        purchaseOrderReqVOS.add(new PurchaseOrderReqVO(322L, 88L));
        System.out.println(redisUtils.lSet("20241005", purchaseOrderReqVOS));
        System.out.println(redisUtils.lGet("20241005", 0, -1));
    }

    /**
     * 移除N个值为value
     */
    @Test
    public void test30() {
        System.out.println(redisUtils.lRemove("20241004", 1, new PurchaseOrderReqVO(189L, 87L)));
        System.out.println(redisUtils.lGet("20241004",0,-1));
    }

    /**
     * ZSet中添加元素
     * 获取ZSet中的元素 正序
     * 获取ZSet中的元素 倒序
     */
    @Test
    public void test31() {
        System.out.println(redisUtils.zSetAdd("20241006", new PurchaseOrderReqVO(186L, 87L), 10));
        System.out.println(redisUtils.zSetAddScore("20241006", new PurchaseOrderReqVO(182L, 87L), 3));
        System.out.println(redisUtils.rangeByScore("20241006",0,100));
        System.out.println(redisUtils.reverseRangeByScore("20241006",0,100));
    }

    /**
     * 删除ZSet中的元素
     */
    @Test
    public void test33() {
        System.out.println(redisUtils.zSetRemove("20241006", new PurchaseOrderReqVO(186L, 87L), 10));
        System.out.println(redisUtils.rangeByScore("20241006",0,100));
    }

}

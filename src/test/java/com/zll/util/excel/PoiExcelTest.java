package com.zll.util.excel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.util.ListUtils;
import com.zll.entity.excelData.ExcelDate;
import com.zll.entity.excelData.ExcelDate02;
import com.zll.entity.excelData.SaleSettlement;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/12/19 13:52:11
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class PoiExcelTest {
    String filepath="E:\\excel\\";

    /**
     * POI导出excel 07版本excel
     */
    @Test
    public void test() {
        //1.创建一个工作簿
        XSSFWorkbook workbook = new XSSFWorkbook();

        //2.创建一个工作表
        Sheet sheet = workbook.createSheet("销售报表");

        //3.创建行
        Row row1 = sheet.createRow(0);
        Row row2 = sheet.createRow(1);
        Row row3 = sheet.createRow(2);

        //4.创建单元格
        //第一行第一个单元格
        Cell cell11 = row1.createCell(0);
        //第一行第二个单元格
        Cell cell12 = row1.createCell(1);
        ///第一行第三个单元格
        Cell cell13 = row1.createCell(2);
        //第二行第一个单元格
        Cell cell21 = row2.createCell(0);
        //第二行第二个单元格
        Cell cell22 = row2.createCell(1);
        ///第二行第三个单元格
        Cell cell23 = row2.createCell(2);
        //第三行第一个单元格
        Cell cell31 = row3.createCell(0);
        //第三行第二个单元格
        Cell cell32 = row3.createCell(1);
        ///第三行第三个单元格
        Cell cell33 = row3.createCell(2);

        //5.设置单元格的值
        cell11.setCellValue("张三");
        cell12.setCellValue("23");
        cell13.setCellValue("计算机");
        cell21.setCellValue("李四");
        cell22.setCellValue("22");
        cell23.setCellValue("数学");
        cell31.setCellValue("王五");
        cell32.setCellValue("29");
        cell33.setCellValue("语文");

        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(filepath + "销售汇总.xlsx");
            workbook.write(fileOutputStream);
            fileOutputStream.close();
            workbook.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * POI 07版本批量写入优化
     */
    @Test
    public void test02() throws Exception{
        // SXSSFWorkbook() 可提升性能
        Workbook workbook = new SXSSFWorkbook();
        Sheet sheet = workbook.createSheet("大文件导出优化测试");
        long begin = System.currentTimeMillis();
        for (int rowNum = 0; rowNum <65536 ; rowNum++) {
            //行
            Row row = sheet.createRow(rowNum);
            for (int cellNum = 0; cellNum < 10 ; cellNum++) {
                //单元格
                Cell cell = row.createCell(cellNum);
                cell.setCellValue("("+(rowNum+1) + "," + (cellNum+1)+")");
            }
        }
        FileOutputStream fileOutputStream = new FileOutputStream(filepath + "07版本批量写入优化.xlsx");
        workbook.write(fileOutputStream);
        // 清理临时文件
        ((SXSSFWorkbook) workbook).dispose();
        fileOutputStream.close();
        workbook.close();
        long end = System.currentTimeMillis();
        System.out.println("耗时："+(double)(end-begin)/1000+"秒");
    }

    /**
     * EasyExcel
     */
    @Test
    public void test03() {
        String fileName = "E:\\excel\\" + "simpleWrite" + System.currentTimeMillis() + ".xlsx";
        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        // 如果这里想使用03 则 传入excelType参数即可
        EasyExcel.write(fileName, ExcelDate.class).sheet("测试模板").doWrite(data01(10));
    }

    private List<ExcelDate> data01(long count) {
        List<ExcelDate> list = ListUtils.newArrayList();
        for (int i = 0; i < count; i++) {
            ExcelDate excelDate = new ExcelDate();
            excelDate.setId(i);
            excelDate.setName("小陈"+i);
            excelDate.setAge(18+1);
            excelDate.setSalary(6.66+i);
            excelDate.setInDate(new Date());
            list.add(excelDate);
        }
        return list;
    }

    /**
     * EasyExcel
     */
    @Test
    public void test04() {
        String fileName = "E:\\excel\\" + "simpleWrite" + System.currentTimeMillis() + ".xlsx";
        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        // 如果这里想使用03 则 传入excelType参数即可
        EasyExcel.write(fileName, ExcelDate02.class).sheet("测试模板").doWrite(data02(10));
    }

    private List<ExcelDate02> data02(long count) {
        List<ExcelDate02> list = ListUtils.newArrayList();
        for (int i = 0; i < count; i++) {
            ExcelDate02 excelDate02 = new ExcelDate02();
            excelDate02.setId(i);
            excelDate02.setName("小陈"+i);
            excelDate02.setAge(18+1);
            excelDate02.setBirth(new Date());
            list.add(excelDate02);
        }
        return list;
    }


    private static int rowNum = 0;
    @Test
    public void test05() {
        // 内存中保留 10000 条数据，以免内存溢出，其余写入 硬盘
        SXSSFWorkbook workbook = new SXSSFWorkbook(1000);
        Sheet sheet = workbook.createSheet("销售结算单");
        long begin = System.currentTimeMillis();
        CellStyle titleStyle = createTitleCellStyle(workbook);
        CellStyle headerStyle = createHeadCellStyle(workbook);
        CellStyle contentStyle = createContentCellStyle(workbook);
        // 行号
        Row tempRow = null;
        Cell tempCell = null;

        rowNum = 0;

        // 创建第一页的第一行，索引从0开始
        tempRow = sheet.createRow(rowNum++);
        tempRow.setHeight((short) 600);// 设置行高

        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 7));//标题单元格合并
        //标题
        String title = "方正镁业有限公司销售结算单";
        tempCell = tempRow.createCell(0);
        tempCell.setCellValue(title);
        tempCell.setCellStyle(titleStyle);

        //合并第2行
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 1));//单位单元格合并
        sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, 6));//结算时间单元格合并
        tempRow = sheet.createRow(rowNum++);
        tempCell = tempRow.createCell(0);
        tempCell.setCellValue("单位：吨，元");
        tempCell.setCellStyle(createSecondCellStyle(workbook, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, true, "宋体",false));
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 2, 7));//时间段单元格合并
        tempCell = tempRow.createCell(2);
        tempCell.setCellValue("结算时间：2024-11-20 11:11:11");
        tempCell.setCellStyle(createSecondCellStyle(workbook, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER, true, "宋体",false));


        tempRow = sheet.createRow(rowNum++);
        tempRow.setHeight((short) 600);// 设置行高
        //合并第三行前6个单元格
        for(int i = 0;i<=6;i++){
            tempCell = tempRow.createCell(i);
            tempCell.setCellValue("客户：方正镁业销售客户1有限公司");
            tempCell.setCellStyle(createSecondCellStyle(workbook, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, false, "宋体",true));
        }
        tempCell = tempRow.createCell(7);
        tempCell.setCellValue("结算周期：2024.08.30-2024.12.30");
        tempCell.setCellStyle(createSecondCellStyle(workbook, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, false, "宋体",true));
        tempRow = sheet.createRow(rowNum++);
        tempRow.setHeight((short) 600);


        sheet.setColumnWidth(0,252*20+323);
        sheet.setColumnWidth(1,252*20+323);
        sheet.setColumnWidth(2,252*20+323);
        sheet.setColumnWidth(3,252*20+323);
        sheet.setColumnWidth(4,252*20+323);
        sheet.setColumnWidth(5,252*20+323);
        sheet.setColumnWidth(6,252*20+323);
        sheet.setColumnWidth(7,252*40+323);


        String[] head = {"货物名称","车数","结算数量","结算单价(元/吨)(税率13%)","结算金额(不含税)","税额","结算金额(含税)(税率13%)","备注"};
        for (int i = 0; i < head.length; i++) {
            tempCell = tempRow.createCell(i);
            tempCell.setCellValue(head[i]);
            tempCell.setCellStyle(headerStyle);
        }

        //内容
        List<SaleSettlement> saleSettlements = new ArrayList<>();
        saleSettlements.add(new SaleSettlement("混煤","50","1520.22","360.00","139087.00","230","136903","结算"));
        saleSettlements.add(new SaleSettlement("混煤","12","520.22","400.00","139000.00","278","136938","结算2"));
        saleSettlements.add(new SaleSettlement("面煤","45","152.22","360.55","139087.24","230","136903","结算3"));
        Integer sum = 0;
        //表格数据
        if (CollectionUtils.isNotEmpty(saleSettlements) && saleSettlements.size() > 0) {
            if (saleSettlements.size() <= 10) {
                sum = 10;
                for (int i = 0; i < saleSettlements.size(); i++) {
                    tempRow = sheet.createRow(rowNum++);
                    tempCell = tempRow.createCell(0);
                    tempCell.setCellValue(saleSettlements.get(i).getProductName());
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(1);
                    tempCell.setCellValue(saleSettlements.get(i).getCarSum());
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(2);
                    tempCell.setCellValue(saleSettlements.get(i).getSettlementNumber());
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(3);
                    tempCell.setCellValue(saleSettlements.get(i).getSettlementPrice());
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(4);
                    tempCell.setCellValue(saleSettlements.get(i).getSettlementMoney());
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(5);
                    tempCell.setCellValue(saleSettlements.get(i).getTheAmountOfTaxPayable());
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(6);
                    tempCell.setCellValue(saleSettlements.get(i).getSettlementAmount());
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(7);
                    tempCell.setCellValue(saleSettlements.get(i).getRemarks());
                    tempCell.setCellStyle(contentStyle);

                }
                //不足10行,补齐10行
                for (int i = saleSettlements.size(); i < sum; i++) {
                    tempRow = sheet.createRow(rowNum++);
                    tempCell = tempRow.createCell(0);
                    tempCell.setCellValue("");
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(1);
                    tempCell.setCellValue("");
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(2);
                    tempCell.setCellValue("");
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(3);
                    tempCell.setCellValue("");
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(4);
                    tempCell.setCellValue("");
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(5);
                    tempCell.setCellValue("");
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(6);
                    tempCell.setCellValue("");
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(7);
                    tempCell.setCellValue("");
                    tempCell.setCellStyle(contentStyle);

                }
            } else {
                sum = saleSettlements.size();
                for (int i = 0; i < sum; i++) {
                    tempRow = sheet.createRow(rowNum++);
                    tempCell = tempRow.createCell(0);
                    tempCell.setCellValue(saleSettlements.get(i).getProductName());
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(1);
                    tempCell.setCellValue(saleSettlements.get(i).getCarSum());
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(2);
                    tempCell.setCellValue(saleSettlements.get(i).getSettlementNumber());
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(3);
                    tempCell.setCellValue(saleSettlements.get(i).getSettlementPrice());
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(4);
                    tempCell.setCellValue(saleSettlements.get(i).getSettlementMoney());
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(5);
                    tempCell.setCellValue(saleSettlements.get(i).getTheAmountOfTaxPayable());
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(6);
                    tempCell.setCellValue(saleSettlements.get(i).getSettlementAmount());
                    tempCell.setCellStyle(contentStyle);

                    tempCell = tempRow.createCell(7);
                    tempCell.setCellValue(saleSettlements.get(i).getRemarks());
                    tempCell.setCellStyle(contentStyle);

                }
            }
        }
        //说明
        tempRow = sheet.createRow(rowNum++);
        tempRow.setHeight((short) 600);// 设置行高
        sheet.addMergedRegion(new CellRangeAddress(sum + 4, sum + 4, 0, 7));//标题单元格合并
        //
        String instructions = "双方约定";
        tempCell = tempRow.createCell(0);
        tempCell.setCellValue(instructions);
        tempCell.setCellStyle(createSecondCellStyle(workbook, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, true, "宋体",false));

        //动态获取
        List<String> list = new ArrayList<>();
        list.add("负责人:");
        list.add("负责人2:");
        list.add("负责人3:");
        list.add("负责人4:");
        list.add("负责人5:");
        list.add("负责人6:");
        list.add("负责人7:");
        list.add("负责人8:");
        list.add("负责人9:");
        tempRow = sheet.createRow(rowNum++);
        tempRow.setHeight((short) 600);
        if (CollectionUtils.isNotEmpty(list) && list.size() <=4) {
            for (int i = 0; i<list.size();i++) {
                sheet.addMergedRegion(new CellRangeAddress(sum + 5, sum + 5, 2 * i, 2 * i + 1));
                tempCell = tempRow.createCell(2 * i);
                tempCell.setCellValue(list.get(i));
                tempCell.setCellStyle(createSecondCellStyle(workbook, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, true, "宋体",false));
            }
        } else if (CollectionUtils.isNotEmpty(list) && list.size() >4 && list.size()<=8) {
            for (int i = 0; i<4;i++) {
                sheet.addMergedRegion(new CellRangeAddress(sum + 5, sum + 5, 2 * i, 2 * i + 1));
                tempCell = tempRow.createCell(2 * i);
                tempCell.setCellValue(list.get(i));
                tempCell.setCellStyle(createSecondCellStyle(workbook, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, true, "宋体",false));
            }
            tempRow = sheet.createRow(rowNum++);
            tempRow.setHeight((short) 600);
            for (int i = 4; i< list.size();i++) {
                sheet.addMergedRegion(new CellRangeAddress(sum + 6, sum + 6, 2 * i - 8, 2 * i - 8 + 1));
                tempCell = tempRow.createCell(2 * i - 8);
                tempCell.setCellValue(list.get(i));
                tempCell.setCellStyle(createSecondCellStyle(workbook, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, true, "宋体",false));
            }
        } else if (CollectionUtils.isNotEmpty(list) && list.size() > 8) {
            for (int i = 0; i<4;i++) {
                sheet.addMergedRegion(new CellRangeAddress(sum + 5, sum + 5, 2 * i, 2 * i + 1));
                tempCell = tempRow.createCell(2 * i);
                tempCell.setCellValue(list.get(i));
                tempCell.setCellStyle(createSecondCellStyle(workbook, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, true, "宋体",false));
            }
            tempRow = sheet.createRow(rowNum++);
            tempRow.setHeight((short) 600);
            for (int i = 4; i< 8;i++) {
                sheet.addMergedRegion(new CellRangeAddress(sum + 6, sum + 6, 2 * i - 8, 2 * i - 8 + 1));
                tempCell = tempRow.createCell(2 * i - 8);
                tempCell.setCellValue(list.get(i));
                tempCell.setCellStyle(createSecondCellStyle(workbook, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, true, "宋体",false));
            }
            tempRow = sheet.createRow(rowNum++);
            tempRow.setHeight((short) 600);
            for (int i = 8; i< list.size();i++) {
                sheet.addMergedRegion(new CellRangeAddress(sum + 7, sum + 7, 2 * i - 16, 2 * i - 16 + 1));
                tempCell = tempRow.createCell(2 * i - 16);
                tempCell.setCellValue(list.get(i));
                tempCell.setCellStyle(createSecondCellStyle(workbook, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, true, "宋体",false));
            }
        }


        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(filepath + "销售结算22.xlsx");
            workbook.write(fileOutputStream);
            fileOutputStream.close();
            workbook.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }




    /**
     * 创建标题样式
     * @param wb
     * @return
     */
    private static CellStyle createTitleCellStyle(SXSSFWorkbook wb) {
        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);//水平居中
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);//垂直对齐
        cellStyle.setFillPattern(FillPatternType.NO_FILL);
//        cellStyle.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());//背景颜色

        Font headerFont1 =  wb.createFont(); // 创建字体样式
        headerFont1.setBold(true); //字体加粗
        headerFont1.setFontName("宋体"); // 设置字体类型
        headerFont1.setFontHeightInPoints((short) 18); // 设置字体大小
        cellStyle.setFont(headerFont1); // 为标题样式设置字体样式

        return cellStyle;
    }

    /**
     * 创建行样式
     * @param wb
     * @param alignment 水平对齐方式
     * @param verticalAlignment 垂直对齐方式
     * @param isBold 字体是否加粗
     * @param fontType 字体类型
     * @param isBorder 是否有边框
     * @return
     */
    private static CellStyle createSecondCellStyle(SXSSFWorkbook wb,HorizontalAlignment alignment, VerticalAlignment verticalAlignment, Boolean isBold, String fontType, Boolean isBorder) {
        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(alignment);
        cellStyle.setVerticalAlignment(verticalAlignment);
        if (isBorder) {
            cellStyle.setBorderBottom(BorderStyle.THIN); //下边框
            cellStyle.setBorderLeft(BorderStyle.THIN); //左边框
            cellStyle.setBorderRight(BorderStyle.THIN); //右边框
            cellStyle.setBorderTop(BorderStyle.THIN); //上边框
        }
        Font headerFont1 =  wb.createFont(); // 创建字体样式
        headerFont1.setBold(isBold); //字体加粗
        headerFont1.setFontName(fontType); // 设置字体类型
        cellStyle.setFont(headerFont1); // 为标题样式设置字体样式
        return cellStyle;
    }

    /**
     * 创建表头样式
     * @param wb
     * @return
     */
    private static CellStyle createHeadCellStyle(SXSSFWorkbook wb) {
        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setWrapText(true);// 设置自动换行
        cellStyle.setAlignment(HorizontalAlignment.CENTER); //水平居中
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER); //垂直对齐
        cellStyle.setFillPattern(FillPatternType.NO_FILL);
        cellStyle.setBorderBottom(BorderStyle.THIN); //下边框
        cellStyle.setBorderLeft(BorderStyle.THIN); //左边框
        cellStyle.setBorderRight(BorderStyle.THIN); //右边框
        cellStyle.setBorderTop(BorderStyle.THIN); //上边框

        Font headerFont = wb.createFont(); // 创建字体样式
        headerFont.setFontName("宋体"); // 设置字体类型
        headerFont.setFontHeightInPoints((short) 12); // 设置字体大小
        cellStyle.setFont(headerFont); // 为标题样式设置字体样式

        return cellStyle;
    }

    /**
     * 创建内容样式
     * @param wb
     * @return
     */
    private static CellStyle createContentCellStyle(SXSSFWorkbook wb) {
        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);// 垂直居中
        cellStyle.setAlignment(HorizontalAlignment.CENTER);// 水平居中
        cellStyle.setWrapText(true);// 设置自动换行
        cellStyle.setBorderBottom(BorderStyle.THIN); //下边框
        cellStyle.setBorderLeft(BorderStyle.THIN); //左边框
        cellStyle.setBorderRight(BorderStyle.THIN); //右边框
        cellStyle.setBorderTop(BorderStyle.THIN); //上边框

        // 生成12号字体
        Font font = wb.createFont();
        font.setColor((short)8);
        font.setFontHeightInPoints((short) 12);
        cellStyle.setFont(font);
        return cellStyle;
    }
}

package com.zll.util.compare;

import com.zll.entity.employee.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/08/28 17:05:21
 */

@SpringBootTest
@RunWith(SpringRunner.class)
public class CompareUtilsTest {
    @Test
    public void compareUtilsTest() {
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setName("张三1");
        employee.setAge(18);
        employee.setPhone(Arrays.asList("12345678901","12345678902","12345678903"));
        Employee employee2 = new Employee();
        employee2.setId(12L);
        employee2.setName("张三12");
        employee2.setAge(19);
        employee2.setPhone(Arrays.asList("12345678901","12345678902","12345678904"));
        String compare = new CompareUtils<Employee>().compare(employee, employee2);
        System.out.println(compare);
    }
}

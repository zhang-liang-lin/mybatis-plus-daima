package com.zll.eventbus;

import com.zll.entity.student.Student;
import com.zll.subscribe.StudentEvent;
import org.greenrobot.eventbus.EventBus;
import org.junit.Test;

/**
 * @description:
 * @author: 八亿科技
 * @date: 2024/10/09 14:15:40
 */
public class EventBusTest {
    @Test
    public void test() {
        Student student = Student.builder().id(1L).name("yrl").build();
        StudentEvent studentEvent = StudentEvent.builder().student(student).build();
        //发布事件
        EventBus.getDefault().post(studentEvent);
    }
}

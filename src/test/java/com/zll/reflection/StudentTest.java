package com.zll.reflection;

import com.baomidou.mybatisplus.annotation.TableId;
import com.zll.entity.student.Student;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @description: 反射
 * @author: 八亿科技
 * @date: 2024/08/28 11:16:58
 */

/**
 * 获取Class对象的三种方式
 * 1 Object ——> getClass();
 * 2 任何数据类型（包括基本数据类型）都有一个“静态”的class属性
 * 3 通过Class类的静态方法：forName（String  className）(常用)
 *
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class StudentTest {
    @Test
    public void studentTest() {
        Student student = Student.builder().id(1L).name("zll").build();
        Class<? extends Student> aClass = student.getClass();
        System.out.println(aClass);  //class com.zll.entity.student.Student
        Class<Student> studentClass = Student.class;
        System.out.println(aClass == studentClass); //true
        try {
            Class<?> aClass1 = Class.forName("com.zll.entity.student.Student");//包的全路径加类名
            System.out.println(aClass1 == aClass); //true
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        //clazz.getName(); 获取类的名称
        System.out.println(aClass.getName()); //com.zll.entity.student.Student
        //clazz.getModifiers() 获取类的修饰符
        System.out.println("修饰符:"+aClass.getModifiers());
        //getSuperclass() 获取类的父类
        Class<?> superclass = aClass.getSuperclass();
        System.out.println("父类:"+superclass);
        //getInterfaces() 获取类的接口
        Class<?>[] interfaces = aClass.getInterfaces();
        System.out.println("接口:" + interfaces);
        //获取类的公有构造方法
        Constructor<?>[] constructors = aClass.getConstructors();
        System.out.println("公有构造方法:" + constructors);
        //获取类的公有方法
        Method[] methods = aClass.getMethods();
        for (Method m : methods) {
            System.out.println("公有方法:" + m);
        }
        //获取某个类及其父类中所有的公有字段。公有字段是指用public修饰的字段
        Field[] fields = aClass.getFields();
        for (Field field : fields) {
            System.out.println("公有字段:" + field);
        }
        //获取某个类中声明的所有字段，包括公有、私有、受保护的字段，但不包括继承的字段
        Field[] declaredFields = aClass.getDeclaredFields();
        System.out.println(aClass.getDeclaredFields()); //[Ljava.lang.reflect.Field;@515ab3f2
        for (Field field : declaredFields) {
            System.out.println(field);//private java.lang.Long com.zll.entity.student.Student.id private java.lang.String com.zll.entity.student.Student.name
            System.out.println("所有字段:" + field.getName()); //id name
            //它用于设置反射对象（Field 对象）的可访问标志。当设置为 true 时，原本不可访问的私有字段将允许访问。 private修饰的也可访问
            field.setAccessible(true);
            try {
                //Field.get() 取得对象的Field属性值 根据属性获取值
                System.out.println("属性值:" + field.get(student));
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
            TableId property = field.getAnnotation(TableId.class);
            //获取该成员变量上指定的注解
            System.out.println("注解:"+property);
        }
        //获取所有构造函数
        Constructor<?>[] constructors1 = aClass.getDeclaredConstructors();
        for (Constructor constructor : constructors1) {
            System.out.println("所有构造函数:" + constructor);
        }
        //获取所有方法
        Method[] declaredMethods = aClass.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            System.out.println("所有方法:" + declaredMethod);
        }
    }
}
